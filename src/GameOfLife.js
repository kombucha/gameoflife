(function() {
    // TODO : Revert x and y where applicable
    // TODO : Fix tests

    var canvasEl = document.querySelector('#game'),
        ctx = canvasEl.getContext('2d'),
        cellColor = '#0f0',
        bgColor = '#000',

        playButton  = document.querySelector('#play'),
        pauseButton = document.querySelector('#pause'),
        stepButton  = document.querySelector('#step'),
        resetButton = document.querySelector('#reset'),
        speedRange  = document.querySelector('#speed'),

        grid,
        gridsPool = [],
        width = 150,
        height = 80,
        cellSize = 8,

        minSpeed = 16,
        maxSpeed = 1000,
        speed = minSpeed,
        setup = false,
        running = false,
        lastStep = Date.now();

    // Game lifecycle methods
    function setupGame () {
        canvasEl.width = width * cellSize;
        canvasEl.height = height * cellSize;

        playButton.addEventListener('click', startGame);
        pauseButton.addEventListener('click', pauseGame);
        stepButton.addEventListener('click', frame);
        resetButton.addEventListener('click', resetGame);

        bindSpeedRange(speedRange);
        bindDrawing(canvasEl);

        initGridPool(width, height);
        grid = getGrid();

        render();
    }

    function startGame () {
        if (running) {
            return;
        }

        running = true;
        lastStep = Date.now();
        window.requestAnimationFrame(frame);
    }

    function pauseGame () {
        running = false;
    }

    function resetGame() {
        pauseGame();
        recycleGrid(grid);
        grid = getGrid();
        render();
    }

    function frame () {
        if ((Date.now() - lastStep) >= speed) {
            update();
            render();
            lastStep = Date.now();
        }

        if (running) {
            window.requestAnimationFrame(frame);
        }
    }

    function update () {
        var newGrid = computeStep(grid);
        recycleGrid(grid);
        grid = newGrid;
    }

    function render () {
        drawGrid(grid);
    }

    // Event handling
    function bindDrawing (canvasEl) {
        canvasEl.addEventListener('click', function onCanvasClick (event) {
            var x = Math.floor((event.x - canvasEl.offsetLeft) / cellSize),
                y = Math.floor((event.y - canvasEl.offsetTop) / cellSize);

            grid[y][x] = !grid[y][x];
            render();
        });
    }

    function bindSpeedRange(range) {
        speedRange.value = minSpeed + (maxSpeed - speed);
        speedRange.attributes.min = minSpeed;
        speedRange.attributes.max = maxSpeed;
        speedRange.addEventListener('change', function onSpeedChange () {
            speed = minSpeed + (maxSpeed - speedRange.value);
        });
    }

    // Game Logic
    function computeStep (grid) {
        var result,
            maxX = grid.length,
            maxY = grid[0].length,
            x, y, line, cell, liveNeighboursCount;

        result = getGrid();

        for (y = 0; y < height; y++) {
            for (x = 0; x < width; x++) {
                cell = grid[y][x];
                liveNeighboursCount = countLiveNeighbours(y, x, grid);

                if (cell) {
                    result[y][x] = (liveNeighboursCount >= 2 && liveNeighboursCount <= 3);
                } else {
                    result[y][x] = (liveNeighboursCount === 3);
                }
            }
        }

        return result;
    }

    function countLiveNeighbours (x, y, grid) {

        var rowSize = grid[0].length,
            columnSize = grid.length,
            subGridRowMin = (y - 1 > -1) ? y - 1 : 0,
            subGridRowMax = (y + 1 < rowSize) ? y + 1 : rowSize-1 ,
            subGridColumnMin = (x - 1 > -1) ? x - 1 : 0,
            subGridColumnMax = (x + 1 < columnSize ) ? x + 1 : columnSize-1 ,
            result = 0;

        for (var j = subGridRowMin; j <= subGridRowMax; j += 1) {
            for (var i = subGridColumnMin; i <= subGridColumnMax; i += 1) {
                if (i !== x || j !== y) {
                    if (grid[i][j]) {
                        result++;
                    }
                }
            }
        }

        return result;

    }

    // Drawing
    function drawGrid (grid) {
        var x, y, posX, posY;

        // Clear board
        ctx.fillStyle = bgColor;
        ctx.fillRect(0, 0, canvasEl.width, canvasEl.height);

        ctx.fillStyle = cellColor;
        for (y = 0; y < height; y++) {
            for (x = 0; x < width; x++) {
                if (grid[y][x]) {
                    posX = x * cellSize;
                    posY = y * cellSize;
                    ctx.fillRect(posX, posY, cellSize, cellSize);
                }
            }
        }
    }

    // Grids handling
    function getGrid () {
        return gridsPool.pop();
    }

    function initGridPool(width, height, poolSize) {
        poolSize = Math.abs(poolSize) || 3;

        var x, y, arr;

        while(gridsPool.length < poolSize) {
            arr = [];
            for (y = 0; y < height; y++) {
                arr[y] = [];
                for (x = 0; x < width; x++) {
                    arr[y][x] = !(Math.floor(Math.random() * 100) % 7);
                }
            }

            gridsPool.push(arr);
        }
    }

    function recycleGrid(grid) {
        if(!grid) {
            return;
        }

        var x, y;

        for (y = 0; y < height; y++) {
            for (x = 0; x < width; x++) {
                grid[y][x] = false;
            }
        }

        gridsPool.push(grid);
    }

    setupGame();
})();
